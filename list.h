//
// Created by Administrator on 2015/5/27.
//

#ifndef SJJG_LIST_H
#define SJJG_LIST_H

#endif //SJJG_LIST_H


#define MAXSIZE 20

#define ERROR -1
#define OK 1
typedef int ElemType;
typedef int Status;

typedef struct {
    ElemType data[MAXSIZE];
    int length; //线性表当前长度
} SqList;